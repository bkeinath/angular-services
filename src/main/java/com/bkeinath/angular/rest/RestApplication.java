package com.bkeinath.angular.rest;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import com.bkeinath.angular.service.greeting.GreetingRestService;
import com.bkeinath.angular.service.test.TestRestService;

@ApplicationPath("/rest")
public class RestApplication extends Application {

	@Override
	public Set<Object> getSingletons() {
		Set<Object> singletons = new HashSet<>();
		//add providers here...
		return singletons;
	}
	
	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> classes = new HashSet<>();
		classes.add(TestRestService.class);
		classes.add(GreetingRestService.class);
		return classes;
	}
}