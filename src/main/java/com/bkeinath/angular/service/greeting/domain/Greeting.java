package com.bkeinath.angular.service.greeting.domain;

public class Greeting {

	private int id;
	private String content;
	
	public Greeting(int id, String name) {
		this.id = id;
		this.content = "Hello, " + name + "!";
	}

	public int getId() {
		return id;
	}

	public String getContent() {
		return content;
	}
	
}
