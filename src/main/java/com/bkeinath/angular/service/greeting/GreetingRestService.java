package com.bkeinath.angular.service.greeting;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.bkeinath.angular.service.greeting.domain.Greeting;
import com.google.gson.Gson;

@Path("/greeting")
@Produces(MediaType.APPLICATION_JSON)
public class GreetingRestService {

	private static final org.apache.logging.log4j.Logger log = org.apache.logging.log4j.LogManager.getLogger(GreetingRestService.class);
	
	@GET
	@Path("hello")
	public Response getValue(@QueryParam("id") String id, @QueryParam("name") String name) {
		Greeting greeting = new GreetingServiceImpl().getGreeting(Integer.parseInt(id), name);
		
		log.info("Greeting created!");
		
		Gson gson = new Gson();
		return Response.ok(gson.toJson(greeting)).build();
	}
	
}
