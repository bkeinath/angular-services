package com.bkeinath.angular.service.greeting;

import com.bkeinath.angular.service.greeting.domain.Greeting;

public class GreetingServiceImpl implements GreetingService {

	@Override
	public Greeting getGreeting(int id, String name) {
		return new Greeting(id, name);
	}
}
