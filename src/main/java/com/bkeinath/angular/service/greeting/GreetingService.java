package com.bkeinath.angular.service.greeting;

import com.bkeinath.angular.service.greeting.domain.Greeting;

public interface GreetingService {

	Greeting getGreeting(int id, String name);

}
