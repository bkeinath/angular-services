package com.bkeinath.angular.service.test;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.bkeinath.utils.time.TimeUtil;

@Path("/test")
@Produces(MediaType.APPLICATION_JSON)
public class TestRestService {

	private static final org.apache.logging.log4j.Logger log = org.apache.logging.log4j.LogManager.getLogger(TestRestService.class);
	
	@GET
	@Path("value")
	public Response getValue(@QueryParam("test") String test) {
		log.info("Value received!");
		
		StringBuilder sb = new StringBuilder("test - ");
		sb.append(test);
		return Response.ok(sb.toString()).build();
	}
	
	@GET
	@Path("time")
	public Response getTime() {
		log.info("Getting time!");
		StringBuilder sb = new StringBuilder("{Time : ");
		
		sb.append(TimeUtil.getDisplayableZonedDateTimeString(TimeUtil.getNowEasternZonedDateTime())).append("}");
		return Response.ok(sb.toString()).build();
	}
}
